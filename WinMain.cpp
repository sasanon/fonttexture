#include "DialogMain.h"

////////////////////////////////////////////////////////////////////////////////
// 
// ■ アプリケーションエントリポイント
// ・このファイルは基本的に変更する必要がありません
// ・ダイアログボックスを作成し、ループを回してメッセージを送る機構です
// 
////////////////////////////////////////////////////////////////////////////////

int WINAPI WinMain(HINSTANCE hCurInst, HINSTANCE hPrevInst, LPSTR lpsCmdLine, int nCmdShow){
		TCHAR szClassName[32] = TEXT("Dialog");
		HINSTANCE hInst = hCurInst;
		HWND hWnd;
		// 作成ウィンドウの設定値を登録
		WNDCLASSEX wc = {sizeof(WNDCLASSEX), 0,NULL, 0, DLGWINDOWEXTRA, hInst, 
						 (HICON)LoadImage(NULL, MAKEINTRESOURCE(IDI_APPLICATION),
						 IMAGE_ICON, 0, 0, LR_DEFAULTSIZE | LR_SHARED),
						 (HCURSOR)LoadImage(NULL, MAKEINTRESOURCE(IDC_ARROW),
						 IMAGE_CURSOR, 0, 0, LR_DEFAULTSIZE | LR_SHARED),
						 (HBRUSH)GetStockObject(WHITE_BRUSH), NULL, 
						 (LPCTSTR)szClassName,
						 (HICON)LoadImage(NULL, MAKEINTRESOURCE(IDI_APPLICATION),
						 IMAGE_ICON, 0, 0, LR_DEFAULTSIZE |LR_SHARED),};
		if(!RegisterClassEx(&wc)) return E_FAIL;

		// ウィンドウ作成
		if((hWnd = CreateDialog(hInst, MAKEINTRESOURCE(IDD_DIALOG_MAIN), GetDesktopWindow(), DlgProc)) == NULL) return E_FAIL;

		// 作ったウィンドウを表示
		ShowWindow(hWnd, nCmdShow);
		UpdateWindow(hWnd);

		//MSG構造体(windowsメッセージ受け取り用)はゼロクリアしておく
		MSG msg;
		ZeroMemory( &msg, sizeof(msg) );
		while(GetMessage(&msg, NULL, 0, 0)){
			// windowsメッセージを取得して、あるならそれを処理(ダイアログベースではIsDialogMessage)
			if(!IsDialogMessage(hWnd,&msg)){
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
			Sleep(20);
		}
		return (INT)msg.wParam;
	}
