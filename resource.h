////////////////////////////////////////////////////////////////////////////////
// 
// ■ コントロールの識別子定義
// ・必要に応じてこのファイルをカスタマイズします
// ・resource.rcのボタン等とDialogMain.hの処理を括りつける一意な識別子(ID)です
// 
////////////////////////////////////////////////////////////////////////////////

#define IDD_DIALOG_MAIN                         1000
#define IDC_TEXTFOLDER_CAPS                     2000
#define IDC_TEXTFOLDER_PATH                     2001
#define IDC_FONT_CAPS                           2002
#define IDC_FONT_NAME                           2003
#define IDC_TEXTFOLDER_CHG                      3000
#define IDC_FONT_CHG                            3001
#define IDC_CREATE_BITMAP                       3002
