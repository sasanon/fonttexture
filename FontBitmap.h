#pragma once
#include "File/File.h"

#include <string>
#include <windows.h>
#include <tchar.h>
#include <algorithm>

// 使用文字glyphsを抽出
void getUseCharacters(std::wstring& glyphs, const std::string& folderpath, const std::vector<std::string>& filelist){
	glyphs.clear();
	setlocale(LC_ALL,"japanese");	// mbstowcs_s用
	for(auto filename : filelist){
		unsigned long filesize = readFileArc(FILEINFO((folderpath+'\\'+filename).data()));
		wchar_t *wcs = new wchar_t[filesize + 1];
		size_t transsize;
		mbstowcs_s(&transsize, wcs, filesize + 1, (char*)fileBuffer, _TRUNCATE);
		memset(&wcs[transsize-1], 0, sizeof(wchar_t));

		unsigned long i, imax, j, jmax;
		for(i=0, imax=filesize; i<imax && wcs[i]!=L'\0'; i++){
			for(j=0, jmax=glyphs.size(); j<jmax && glyphs[j]!=wcs[i]; j++);
			if(wcs[i] == L'\r') continue;
			if(wcs[i] == L'\n') continue;
			if(wcs[i] == L'\t') continue;
			if(j != jmax)       continue;
			glyphs.push_back(wcs[i]);
		}
		delete[] wcs;
	}
	std::sort(glyphs.begin(), glyphs.end());
}

//----------------------------------------------------------
//■ビットマップ全データ(ヘッダ含む)書き込み
//----------------------------------------------------------
void WriteData(const LOGFONT& logfont, const std::wstring& glyphs){
	// header /////////////////////////////////////////////////////////////////
	POINT   FontBitmapSize, AllBitmapSize;  // 1フォントの全角サイズ、ビットマップ全体サイズ

	// 1辺文字数と1辺ピクセル数の初期化
	unsigned int CharInLine; for(CharInLine=1; CharInLine*CharInLine<glyphs.size(); CharInLine*=2);
	FontBitmapSize.x = 32, FontBitmapSize.y = 32;
	AllBitmapSize.x = FontBitmapSize.x * CharInLine,
	AllBitmapSize.y = FontBitmapSize.y * CharInLine;

	// ■ビットマップヘッダ書き込み
	// BITMAPFILEHEADER構造体の設定
	BITMAPFILEHEADER* BMPFileHeader = (BITMAPFILEHEADER*)&fileBuffer[0];
	memset(BMPFileHeader, 0, sizeof(BITMAPFILEHEADER)); //初期化
	BMPFileHeader->bfType       = 0x4D42;  // "BM"
	BMPFileHeader->bfOffBits    = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);
	BMPFileHeader->bfSize       = BMPFileHeader->bfOffBits  + AllBitmapSize.x * AllBitmapSize.y * sizeof(RGBQUAD);

	// BITMAPFILEHEADER構造体の設定
	BITMAPINFOHEADER* BMPInfoHeader = (BITMAPINFOHEADER*)&fileBuffer[sizeof(BITMAPFILEHEADER)];
	memset(BMPInfoHeader, 0, sizeof(BITMAPINFOHEADER)); //初期化
	BMPInfoHeader->biSize           = sizeof(BITMAPINFOHEADER);
	BMPInfoHeader->biWidth          = AllBitmapSize.x;
	BMPInfoHeader->biHeight         = AllBitmapSize.y;
	BMPInfoHeader->biPlanes         = 1;
	BMPInfoHeader->biBitCount       = 32;   // フルカラー

	// bitmap /////////////////////////////////////////////////////////////////
	memset(&fileBuffer[BMPFileHeader->bfOffBits], 0, AllBitmapSize.x * AllBitmapSize.y * sizeof(RGBQUAD));	//初期化

	// フォント情報
	HFONT  hFont;
	if ((hFont = CreateFontIndirect(&logfont)) == NULL) return;
	HDC    hdc = GetDC(NULL);
	GLYPHMETRICS GM;
	TEXTMETRIC  TM;
	CONST MAT2  Mat = {{0,1},{0,0},{0,0},{0,1}}; // 文字変換行列(GetGlyphOutline用)
	// データ操作用
	POINT   px, start,end;

	SelectObject(hdc, hFont);       // DCにフォント設定
	GetTextMetrics(hdc, &TM);       // 現在のフォント情報取得

	for(unsigned int i=0; i<glyphs.size(); i++){
		DWORD size = GetGlyphOutlineW(hdc, glyphs[i], GGO_GRAY8_BITMAP, &GM, 0, NULL, &Mat); // 全角サイズ取得(文字毎に異なる)
		PBYTE pSrcData = new BYTE[size];             // 読取ビットマップ領域確保
		// ビットマップ、現在のフォント情報取得
		GetGlyphOutlineW(hdc, glyphs[i], GGO_GRAY8_BITMAP, &GM, size, pSrcData, &Mat);
		GetTextMetrics(hdc, &TM);

		// フォント読込位置(pSrcData内ビット位置)の設定
		start.x = GM.gmptGlyphOrigin.x;                     start.y = TM.tmAscent - GM.gmptGlyphOrigin.y;
		end.x   = GM.gmBlackBoxX + (4-GM.gmBlackBoxX%4)%4;  end.y   = GM.gmBlackBoxY-1;

		// ビットマップデータの変換(64階調グレーから32bitRGB)
		for( px.y = start.y; px.y <= (start.y + end.y); px.y++ ){ // ライン単位ループ
			for( px.x =  start.x; px.x < (start.x + end.x); px.x++){ // ドット単位ループ
				DWORD trans = (pSrcData[end.x * (px.y - start.y) +  px.x - start.x] * 0xff / 64 );	// 64階調グレーを128階調(BYTE)に拡張
				DWORD color = ((0x00000000 | (trans<<16)) | (trans<<8)) | (trans);					// 32bitRGB値に変換
				// RGBQUADに設定
				memcpy( fileBuffer + BMPFileHeader->bfOffBits +
				        (AllBitmapSize.y  * FontBitmapSize.x * ((CharInLine-1-i/CharInLine)) +              // ライン幅  * y文字数 分のオフセット
				         AllBitmapSize.y  * ((FontBitmapSize.y-1)-px.y) + // 文字内yドット数 分のオフセット
				         FontBitmapSize.x * (i%CharInLine) +              // 文字横幅  * x文字数   分のオフセット
				         px.x)                                            // 文字内xドット数 分のオフセット
						 * sizeof(RGBQUAD),
				       &color,
				       sizeof(RGBQUAD) );
			}
		}
		delete [] pSrcData;
	}
	DeleteObject(hFont);

	// ファイル書込
	writeFileArc(FILEINFO("font.bmp",0,BMPFileHeader->bfSize));
}
