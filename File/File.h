#ifndef __FileEx_h__
#define __FileEx_h__

#include <string>
#include <vector>

// 読込ファイル情報
struct FILEINFO{
	FILEINFO(const char* name = "", unsigned long offset = 0, unsigned long size = 0) : name(name), offset(offset), size(size){};
	std::string     name;	// ファイルパス
	unsigned long   offset;	// 読込開始オフセット(ファイル先頭からのバイト数)
	unsigned long   size;	// 読込サイズ
};

// ファイル読み書き用共通バッファ(20MB)
extern unsigned char fileBuffer[20000000];

// ファイル読込(offset/size指定)
extern unsigned long readFileArc (const FILEINFO& arcinfo,       unsigned char* readBuffer  = fileBuffer);

// ファイル書込(offset/size指定)
extern unsigned long writeFileArc(const FILEINFO& arcinfo, const unsigned char* writeBuffer = fileBuffer);

// フォルダ内のファイルをリストアップして取得
void getFilenamesInDirectory(std::vector<std::string>& out, std::string directoryPath, const char* ext);

// ファイルパスからディレクトリパスを取得
std::string getDirectoryA(std::string filePath);
std::wstring getDirectoryW(std::wstring filePath);
// 現在の実行exeのディレクトリパスを取得
std::string getExeDirectoryA();
std::wstring getExeDirectoryW();
#ifdef UNICODE
#define getDirectory getDirectoryW
#define getExeDirectory getExeDirectoryW
#else
#define getDirectory getDirectoryA
#define getExeDirectory getExeDirectoryA
#endif

#endif 
