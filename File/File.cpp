#include "File.h"
#include <windows.h>
#include <stdio.h>

unsigned char fileBuffer[20000000];

//////////////////////////////////////////////////////////////////
// 拡張ファイル制御
///////////////////////////////////////////////////////////////////

// ファイル読込(offset/size指定)
unsigned long readFileArc( const FILEINFO& fileinfo, unsigned char* readBuffer){
	FILE* fp;
	fopen_s(&fp, fileinfo.name.data(), "rb");
	if(fp == NULL)	return 0;

	// ファイルサイズ取得
	fseek(fp, 0, SEEK_END);
	unsigned long tmpsize = ftell(fp);

	// 要求サイズ調整
	unsigned long reqsize = fileinfo.size;
	if(reqsize == 0)                        reqsize = tmpsize - fileinfo.offset;	// 要求サイズ0時は読込開始offset以降を全読込とする
	if(fileinfo.offset + reqsize > tmpsize) reqsize = tmpsize - fileinfo.offset;	// ファイル長を超えるサイズ指定の場合、ファイル長ぎりぎりまでの読込とする

	// 読込
	fseek(fp, fileinfo.offset, SEEK_SET);
	tmpsize = fread(readBuffer, sizeof(unsigned char), reqsize, fp);
	readBuffer[tmpsize] = 0;

	fclose(fp);
	return tmpsize;
}

// ファイル書込(offset/size指定)
unsigned long writeFileArc( const FILEINFO& fileinfo, const unsigned char* writeBuffer){
	if(fileinfo.size == 0)	return 0;	// サイズ0指定時は書き込めない

	FILE* fp;
	fopen_s(&fp, fileinfo.name.data(), "wb");
	if(fp == NULL)	return 0;

	// 書込
	fseek(fp, fileinfo.offset, SEEK_SET);
	unsigned long tmpsize = fwrite(writeBuffer, sizeof(unsigned char), fileinfo.size, fp);

	fclose(fp);
	return tmpsize;
}

// フォルダ内のファイルをリストアップして取得
void getFilenamesInDirectoryRecursion(std::vector<std::string>& out, std::string directoryPath, const char* ext){
	WIN32_FIND_DATAA   lpFind;
	HANDLE              hFind = FindFirstFileA((directoryPath+"\\*").data(), &lpFind);
	if (hFind == INVALID_HANDLE_VALUE){	// 該当データなし
		FindClose(hFind);
		return;
	}
	
	do{
		if(lpFind.cFileName[0] == '.')	continue;
		if(lpFind.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY){
			getFilenamesInDirectoryRecursion(out, directoryPath+"\\"+lpFind.cFileName, ext);	// ディレクトリであった場合は再帰検索する
			continue;
		}
		out.push_back(&(directoryPath+"\\"+lpFind.cFileName).data()[2]);
	} while (FindNextFileA(hFind, &lpFind) == TRUE);
}
void getFilenamesInDirectory(std::vector<std::string>& out, std::string directoryPath, const char* ext){
	char originPath[_MAX_PATH];
	GetCurrentDirectoryA(_MAX_PATH, originPath);
	SetCurrentDirectoryA(directoryPath.data());
	getFilenamesInDirectoryRecursion(out, ".", ext);
	SetCurrentDirectoryA(originPath);
}

std::string getDirectoryA(std::string filePath){
	char szDrive[_MAX_DRIVE], szPath[_MAX_PATH], szDir[_MAX_DIR];
	_splitpath_s(filePath.data(), szDrive,_MAX_DRIVE, szDir,_MAX_DIR, 0,0, 0,0); 
	if(szDir[strlen(szDir)-1] == '\\') szDir[strlen(szDir)-1] = '\0';
	strcpy_s(szPath,szDrive);
	strcat_s(szPath,szDir);
	return szPath;
}
std::wstring getDirectoryW(std::wstring filePath){
	WCHAR szDrive[_MAX_DRIVE], szPath[_MAX_PATH], szDir[_MAX_DIR];
	_wsplitpath_s(filePath.data(), szDrive,_MAX_DRIVE, szDir,_MAX_DIR, 0,0, 0,0); 
	if(szDir[wcslen(szDir)-1] == L'\\') szDir[wcslen(szDir)-1] = L'\0';
	wcscpy_s(szPath,szDrive);
	wcscat_s(szPath,szDir);
	return szPath;
}

std::string getExeDirectoryA(void){
	char szPath[_MAX_PATH];
	if(GetModuleFileNameA(NULL, szPath, sizeof(szPath)) == 0)	return "";
	return getDirectoryA(szPath);
}
std::wstring getExeDirectoryW(void){
	WCHAR szPath[_MAX_PATH];
	if(GetModuleFileNameW(NULL, szPath, sizeof(szPath)) == 0)	return L"";
	return getDirectoryW(szPath);
}
