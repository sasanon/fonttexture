#pragma once
#include <windows.h>
#include "resource.h"
#include "CommonDialog.h"

#include "FontBitmap.h"

////////////////////////////////////////////////////////////////////////////////
// 
// ■ ダイアログボックスの挙動定義
// ・必要に応じてこのファイルをカスタマイズします
// ・ボタン押下等のメッセージを受け取るDlgProc関数(ウィンドウプロシージャ)と
//   その処理部分を外部に出してまとめたDialogEventHandlerクラスからなります
// ・Dialogクラスの処理が長くなる場合は別ファイルに関数を書くなど分割のこと
// 
////////////////////////////////////////////////////////////////////////////////

// ダイアログ イベントハンドラ(ボタン押下等に対応する処理)クラス
class DialogEventHandler{
	HWND hWnd;
	std::string foldername, folderpath;
	LOGFONT logfont;
	COLORREF fontcolor;
public:
	// 初期化
	void OnInit(HWND hSrcWnd){
		hWnd = hSrcWnd;
		folderpath = getExeDirectoryA() + "\\text";
		SetDlgItemTextA(hWnd, IDC_TEXTFOLDER_PATH, folderpath.data());

		HFONT hFont = CreateFontA(24, 0, 0, 0, 0, FALSE, FALSE, FALSE, SHIFTJIS_CHARSET, OUT_TT_ONLY_PRECIS, CLIP_DEFAULT_PRECIS, PROOF_QUALITY, FIXED_PITCH | FF_MODERN, "ＭＳ ゴシック" );
		GetObject(hFont, sizeof(LOGFONT),&logfont);
		DeleteObject(hFont);
		SetDlgItemText(hWnd, IDC_FONT_NAME, logfont.lfFaceName);
	}

	// テキストデータフォルダ変更
	void OnFolderChange(WORD wp){
		if(!OpenCommonDialogFolder(foldername, folderpath, hWnd, "フォルダを選択してください")) return;
		SetDlgItemTextA(hWnd, IDC_TEXTFOLDER_PATH, folderpath.data());
	}

	// フォント変更
	void OnFontChange(WORD wp){
		OpenCommonDiaogFont(logfont, fontcolor, hWnd);
		SetDlgItemText(hWnd, IDC_FONT_NAME, logfont.lfFaceName);
	}

	// BITMAP作成
	void OnCreateBitmap(WORD wp){
		// 指定フォルダfolderpath内のファイルを全てリストアップ
		std::vector<std::string> filelist;
		getFilenamesInDirectory(filelist, folderpath, "*");

		// 使用文字glyphsを抽出
		std::wstring glyphs;
		getUseCharacters(glyphs, folderpath, filelist);

		// 使用文字glyphsをs-jisでファイルに書き出す
		size_t charsize;
		wcstombs_s(&charsize, (char*)fileBuffer, glyphs.length()*2 + 1, glyphs.data(), _TRUNCATE);
		SetCurrentDirectoryA(getExeDirectoryA().data());
		writeFileArc(FILEINFO("font.txt",0,charsize));

		// 使用文字glyphsをbitmapファイルに書き出す
		WriteData(logfont, glyphs);
		MessageBoxA(hWnd, "ラスタライズしたフォントbitmapを出力しました！", "出力完了！", MB_OK);
	}
};
static DialogEventHandler dialog;

//ウィンドウプロシージャ(モードレスダイアログ)
INT_PTR CALLBACK DlgProc(HWND hWnd, UINT msg, WPARAM wp, LPARAM lp){
	// windowsメッセージ毎の処理
	switch (msg){
		case WM_INITDIALOG: dialog.OnInit(hWnd);                break;	//ウインドウ作成時の処理
		case WM_CLOSE:      PostMessage(hWnd,WM_DESTROY,0,0);   break;	//右上[X]ボタン
		case WM_DESTROY:    PostQuitMessage(0);                 break;	//プログラム終了時のウインドウ破棄
		case WM_COMMAND: //ユーザーメッセージ処理
			switch(LOWORD(wp)){
				case IDC_TEXTFOLDER_CHG:    dialog.OnFolderChange(LOWORD(wp));	break;	// "テキストフォルダ変更"ボタン
				case IDC_FONT_CHG:          dialog.OnFontChange(LOWORD(wp));	break;	// "フォント変更"ボタン
				case IDC_CREATE_BITMAP:     dialog.OnCreateBitmap(LOWORD(wp));	break;	// "ビットマップ作成"ボタン
				default:	return FALSE;	// ダイアログベースではDefWindowProcしない
			}
			break;
		default: return FALSE; // ダイアログベースではDefWindowProcしない
	}
	return TRUE;	// 実装したメッセージはbreakしてこのreturnからwindows制御に戻る
}
